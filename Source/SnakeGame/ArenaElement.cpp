// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaElement.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AArenaElement::AArenaElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ArenaElement = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ArenaElement"));
	ArenaElement->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ArenaElement->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AArenaElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AArenaElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
