// Fill out your copyright notice in the Description page of Project Settings.


#include "Arena.h"
#include "ArenaElement.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Engine.h"

// Sets default values
AArena::AArena()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;

}

// Called when the game starts or when spawned
void AArena::BeginPlay()
{
	Super::BeginPlay();
	GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(Score), true, FVector2D(2.f, 2.f));

	SetArenaSize(ArenaWidth, ArenaHeight);
	DrawAllWalls();
	DrawWallFencing();

	SpawnObstacles();

	SetFoodSpawnMap();

	ASnakeBase& SnakeRef = CreateSnakeActor();
	SnakeActor = &SnakeRef;
	SnakeActor->GetArena(this);
	
	SpawnFood(false);

	if (!IsValid(SnakeActor))
	{
		UE_LOG(LogTemp, Error, TEXT("SnakeActor is null."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("SnakeActor is OKAY."));
	}

	if (!IsValid(FoodActor))
	{
		UE_LOG(LogTemp, Error, TEXT("FoodActor is null."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("FoodActor is OKAY."));
	}
}

// Called every frame
void AArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArena::SetArenaSize(int32 Width, int32 Height)
{
	if (Width >= 1 && Height >= 1)
	{
		FVector FirstElementLocation(ElementSize * Width / 2 + 50, -ElementSize * Height / 2 - 50, -ElementSize);
		for (int32 i = 0; i < Height + 2; i++)
		{
			FVector HorizontalLocation(FirstElementLocation + FVector(0, ElementSize * i, 0));
			TArray<FVector> VerticalLines;

			for (int32 j = 0; j < Width + 2; j++)
			{
				FVector VerticalLocation(HorizontalLocation - FVector(ElementSize * j, 0, 0));
				FTransform NewTransform(FRotator(0,0,0), VerticalLocation, FVector(1.f, 1.f, 1.f));
				AArenaElement* NewArenaElement = GetWorld()->SpawnActor<AArenaElement>(ArenaElement, NewTransform);
				VerticalLines.Add(VerticalLocation);
				MapWidth = j;
			}
			ArenaMap.Add(VerticalLines);
			MapHeight = i;
		}
	}
}

void AArena::SpawnFood(bool IsBonusFood)
{
	if (IsBonusFood)
	{
		for (int32 i = 0; i < 2; i++)
		{
			RefreshFoodSpawnMap();
			if (TempFoodSpawnMap.Num() > 0)
			{
				RandomCoordinate = FMath::FRandRange(0, TempFoodSpawnMap.Num());
				FoodActor = GetWorld()->SpawnActor<AFood>(FoodBombSplits, FTransform(TempFoodSpawnMap[RandomCoordinate]));
				FoodActor->SetInitialLocation(TempFoodSpawnMap[RandomCoordinate]);
				FoodActor->bIsBonusFood = true;
				FoodActor->bUsualFood = false;
				FoodActor->GetArena(this);
				FoodActor->GetSnake(SnakeActor);
				BonusesArray.Add(FoodActor);
			}
		}
	}
	else
	{
		RefreshFoodSpawnMap();
		if (TempFoodSpawnMap.Num() > 0)
		{
			RandomCoordinate = FMath::FRandRange(0, TempFoodSpawnMap.Num());
			FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(TempFoodSpawnMap[RandomCoordinate]));
			FoodActor->SetInitialLocation(TempFoodSpawnMap[RandomCoordinate]);
			FoodActor->GetArena(this);
			FoodActor->GetSnake(SnakeActor);
		}
	}
}

void AArena::SpawnBonus(TSubclassOf<AFood> FoodSubclass)
{
	RefreshFoodSpawnMap();
	if (TempFoodSpawnMap.Num() > 0)
	{
		RandomCoordinate = FMath::FRandRange(0, TempFoodSpawnMap.Num());
		FoodActor = GetWorld()->SpawnActor<AFood>(FoodSubclass, FTransform(TempFoodSpawnMap[RandomCoordinate]));
		FoodActor->SetInitialLocation(TempFoodSpawnMap[RandomCoordinate]);
		bBonusIsSpawned = true;
		FoodActor->bIsBonus = true;
		FoodActor->bUsualFood = false;
		if (FoodSubclass == FoodBombBonus)
		{
			FoodActor->bIsFoodBombBonus = true;
		}
		else if (FoodSubclass == PoisonousFood)
		{
			FoodActor->bIsPoisonousFood = true;
		}
		else if (FoodSubclass == SpeedUpBonus)
		{
			FoodActor->bIsSpeedUpBonus = true;
		}
		else if (FoodSubclass == SlowDownBonus)
		{
			FoodActor->bIsSlowDownBonus = true;
		}
		FoodActor->GetArena(this);
		FoodActor->GetSnake(SnakeActor);
		BonusesArray.Add(FoodActor);
		if (NotOne <= One)
		{
			NotOne++;
			GEngine->AddOnScreenDebugMessage(3, 100.0f, FColor::Yellow, FString("Food to refresh bonuses: ") + FString::FromInt(4 - FoodCounter), true, FVector2D(1.5f, 1.5f));
		}
		else if (bBonusIsSpawned)
		{
			GEngine->AddOnScreenDebugMessage(3, 100.0f, FColor::Yellow, FString("Food to refresh bonuses: ") + FString::FromInt(4 - FoodCounter), true, FVector2D(1.5f, 1.5f));
		}
	}
}

void AArena::SpawnBonuses(int32 BonusNumber)
{
	switch (BonusNumber)
	{
	case 1:
		SpawnBonus(FoodBombBonus);
		break;
	case 2:
		SpawnBonus(PoisonousFood);
		break;
	case 3:
		SpawnBonus(SpeedUpBonus);
		break;
	case 4:
		SpawnBonus(SlowDownBonus);
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("Incorrect bonus number!"));
		break;
	}
}

void AArena::SpawnBonuses()
{
	int32 RandomBonus = FMath::FRandRange(1, 5);
	SpawnBonuses(RandomBonus);
}

void AArena::DrawHorizontalWall(int32 LineNum, int32 HorizontalElement, int32 NumOfElements)
{
	int32 NewX = ArenaMap[LineNum][HorizontalElement].X;
	int32 NewY = ArenaMap[LineNum][HorizontalElement].Y;
	int32 NewZ = ArenaMap[LineNum][HorizontalElement].Z;
	for (int32 i = 0; i < NumOfElements; i++)
	{
		FVector NewLocation(NewX, NewY + (100 * i), NewZ + 100);
		FTransform NewTransform(NewLocation);
		ArenaWalls.Add(GetWorld()->SpawnActor<AArenaElement>(ArenaWallElement, NewTransform));
		
		ArenaWallsMap.Add(NewLocation);
	}
}

void AArena::DrawVerticalWall(int32 LineNum, int32 VerticalElement, int32 NumOfElements)
{
	int32 NewX = ArenaMap[LineNum][VerticalElement].X;
	int32 NewY = ArenaMap[LineNum][VerticalElement].Y;
	int32 NewZ = ArenaMap[LineNum][VerticalElement].Z;
	for (int32 i = 0; i < NumOfElements; i++)
	{
		FVector NewLocation(NewX - (100 * i), NewY, NewZ + 100);
		FTransform NewTransform(NewLocation);
		ArenaWalls.Add(GetWorld()->SpawnActor<AArenaElement>(ArenaWallElement, NewTransform));

		ArenaWallsMap.Add(NewLocation);
	}
}

void AArena::DrawAllWalls()
{
	for (int32 i = 0; i < ArenaWallsSet.Num(); i++)
	{
		if (ArenaWallsSet[i].bIsWallVertical && ArenaWallsSet[i].bIsWallHorizontal)
		{
			DrawVerticalWall(ArenaWallsSet[i].VerticalLineNum, ArenaWallsSet[i].ElementToBegin, ArenaWallsSet[i].NumOfElements);
		}
		else if (ArenaWallsSet[i].bIsWallHorizontal)
		{
			DrawHorizontalWall(ArenaWallsSet[i].VerticalLineNum, ArenaWallsSet[i].ElementToBegin, ArenaWallsSet[i].NumOfElements);
		}
		else if (ArenaWallsSet[i].bIsWallVertical)
		{
			DrawVerticalWall(ArenaWallsSet[i].VerticalLineNum, ArenaWallsSet[i].ElementToBegin, ArenaWallsSet[i].NumOfElements);
		}
		else
		{
			continue;
		}
	}
}

void AArena::DrawWallFencing()
{
	DrawVerticalWall(0, 0, MapHeight + 1);
	DrawVerticalWall(MapWidth, 0, MapHeight + 1);
	DrawHorizontalWall(1, 0, MapWidth - 1);
	DrawHorizontalWall(1, MapHeight, MapWidth - 1);
}

void AArena::SpawnObstacles()
{
	for (int32 i = 0; i < ArenaWallsMap.Num(); i++)
	{
		FVector NewVector(ArenaWallsMap[i]);
		FTransform NewTransform(NewVector);
		ASnakeElementBase* NewObstacleElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElemClass, NewTransform);
	}
}

ASnakeBase& AArena::CreateSnakeActor()
{
	ASnakeBase* SnakePtr = GetWorld()->SpawnActor<ASnakeBase>(SnakeClass, FTransform());

	if (IsValid(SnakePtr))
	{
		UE_LOG(LogTemp, Warning, TEXT("SnakeActor created successfully."));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to create SnakeActor."));
	}
	return *SnakePtr;
}

ASnakeBase* AArena::GetSnake()
{
	// TODO: �������� ����� �������� return
	return SnakeActor;
}

void AArena::SetFoodSpawnMap()
{
	for (int32 i = 0; i < ArenaMap.Num(); i++)
	{
		for (int32 j = 0; j < ArenaMap[i].Num(); j++)
		{
			TempArenaMap.Add(ArenaMap[i][j] + FVector(0, 0, ElementSize));
		}
	}

	bool bIsWall;

	for (int32 i = 0; i < TempArenaMap.Num(); i++)
	{
		bIsWall = false;
		for (int32 c = 0; c < ArenaWallsMap.Num(); c++)
		{
			if (TempArenaMap[i] == ArenaWallsMap[c])
			{
				bIsWall = true;
				break;
			}
		}
		if (!bIsWall)
		{
			FoodSpawnMap.Add(TempArenaMap[i]);
		}
	}
	TempFoodSpawnMap = FoodSpawnMap;
}

//Not now

//void AArena::CheckWallsSides()
//{
//	CheckOneSide(1);
//	CheckOneSide(2);
//	CheckOneSide(3);
//	CheckOneSide(4);
//}

//void AArena::CheckOneSide(int32 NumOfSide)
//{
//	FVector SideLocation;
//	for (int32 i = 0; i < ArenaWallsMap.Num(); i++)
//	{
//		switch (NumOfSide)
//		{
//		case 1:
//			SideLocation = ArenaWallsMap[i] + FVector(ElementSize, 0, 0);
//			break;
//		case 2:
//			SideLocation = ArenaWallsMap[i] + FVector(-ElementSize, 0, 0);
//			break;
//		case 3:
//			SideLocation = ArenaWallsMap[i] + FVector(0, ElementSize, 0);
//			break;
//		case 4:
//			SideLocation = ArenaWallsMap[i] + FVector(0, -ElementSize, 0);
//			break;
//		default:
//			break;
//		}
//
//		for (int32 j = 0; j < ArenaWallsMap.Num(); j++)
//		{
//			if (SideLocation == ArenaWallsMap[j])
//			{
//				switch (NumOfSide)
//				{
//				case 1:
//					ArenaWalls[i]->bIsTopElementFree = false;
//					break;
//				case 2:
//					ArenaWalls[i]->bIsDownElementFree = false;
//					break;
//				case 3:
//					ArenaWalls[i]->bIsRightElementFree = false;
//					break;
//				case 4:
//					ArenaWalls[i]->bIsLeftElementFree = false;
//					break;
//				default:
//					break;
//				}
//				break;
//			}
//		}
//	}
//}

//For food map creation check
void AArena::RefreshFoodSpawnMap()
{
	//TArray<FVector> LocationsToDelete;
	
	
	for (int32 i = 0; i < NewArenaElements.Num(); i++)
	{
		NewArenaElements[i]->Destroy();
	}
	
	
	TempFoodSpawnMap.Empty();
	TempFoodSpawnMap = FoodSpawnMap;

	for (int32 i = 0; i < TempFoodSpawnMap.Num(); i++)
	{
		if (CheckBonusesLocation(TempFoodSpawnMap[i]) || CheckSnakeLocation(TempFoodSpawnMap[i]))
		{
			TempFoodSpawnMap.RemoveAt(i, 1, true);
			i--;
		}
	}
}

bool AArena::CheckBonusesLocation(FVector FoodMapLocation)
{
	bool IsEqual = false;
	for (int32 i = 0; i < BonusesArray.Num(); i++)
	{
		if (FoodMapLocation == BonusesArray[i]->InitialLocation)
		{
			IsEqual = true;
			break;
		}
	}
	return IsEqual;
}

bool AArena::CheckSnakeLocation(FVector FoodMapLocation)
{
	bool IsEqual = false;
	for (int32 i = 0; i < SnakeActor->SnakeElements.Num(); i++)
	{
		if (FoodMapLocation == SnakeActor->SnakeElements[i]->GetActorLocation())
		{
			IsEqual = true;
			break;
		}
	}
	return IsEqual;
}

void AArena::FoodMapCreate()
{
	for (int32 i = 0; i < TempFoodSpawnMap.Num(); i++)
	{
		FTransform NewTransform(TempFoodSpawnMap[i] + FVector(0,1500,400));
		NewArenaElements.Add(GetWorld()->SpawnActor<AArenaElement>(ArenaElement, NewTransform)); 
	}
}
