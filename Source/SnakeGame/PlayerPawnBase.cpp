// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Arena.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));

	AArena& ArenaRef = CreateArena();
	ArenaActor = &ArenaRef;

	SnakeActor = ArenaActor->GetSnake();
	TempSpeed = SnakeActor->GetMovementSpeed();

	if (!IsValid(SnakeActor))
	{
		UE_LOG(LogTemp, Error, TEXT("SnakeActor in PLAYER_PAWN is null."));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("SnakeActor in PLAYER_PAWN is OKAY."));
	}
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

	PlayerInputComponent->BindAction("Pause", IE_Released, this, &APlayerPawnBase::PauseGame);
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (SnakeActor->GetMovementSpeed() != 0)
	{
		if (IsValid(SnakeActor))
		{
			if (value > 0 && SnakeActor->CheckNextMove(EMovementDirection::UP))
			{
				SnakeActor->LastMoveDirection = EMovementDirection::UP;
				UE_LOG(LogTemp, Warning, TEXT("Moved UP"));
			}
			else if (value < 0 && SnakeActor->CheckNextMove(EMovementDirection::DOWN))
			{
				SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
				UE_LOG(LogTemp, Warning, TEXT("Moved DOWN"));
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (SnakeActor->GetMovementSpeed() != 0)
	{
		if (IsValid(SnakeActor))
		{
			if (value > 0 && SnakeActor->CheckNextMove(EMovementDirection::RIGHT))
			{
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
				UE_LOG(LogTemp, Warning, TEXT("Moved RIGHT"));
			}
			else if (value < 0 && SnakeActor->CheckNextMove(EMovementDirection::LEFT))
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
				UE_LOG(LogTemp, Warning, TEXT("Moved LEFT"));
			}
		}
	}
}

AArena& APlayerPawnBase::CreateArena()
{
	AArena* ArenaPtr = GetWorld()->SpawnActor<AArena>(ArenaClass, FTransform());

	if (IsValid(ArenaPtr))
	{
		UE_LOG(LogTemp, Warning, TEXT("ArenaActor created successfully."));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to create ArenaActor."));
	}
	return *ArenaPtr;
}

void APlayerPawnBase::PauseGame()
{

	if (IsValid(SnakeActor))
	{
		if (!bGameIsPaused)
		{
			UE_LOG(LogTemp, Warning, TEXT("Game is paused."));
			SnakeActor->SetMovementSpeed(0);
			bGameIsPaused = true;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Game resumed."));
			SnakeActor->SetMovementSpeed(TempSpeed);
			bGameIsPaused = false;
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("GAME CAN'T BE RESUMED, INVALID SNAKE ACTOR!"));
	}
	
}
