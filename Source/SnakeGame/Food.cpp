// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Arena.h"
#include "SnakeElementBase.h"
#include "Engine/Engine.h"

class ASnakeElementBase;

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TempLocation != FVector::ZeroVector)
	{
		FoodAnimation();
	}
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (bIsBonus && !bUsualFood)
			{
				if (bIsFoodBombBonus)
				{
					this->Destroy();
					SnakeActor->AddSnakeElement();
					ArenaActor->RefreshFoodSpawnMap();
					ArenaActor->SpawnFood(true);

					ArenaActor->Score += 100;
					GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
				}
				if (bIsPoisonousFood)
				{
					if (SnakeActor->SnakeElements.Num() >= 5)
					{
						this->Destroy();
						for (int32 i = 0; i < 3; i++)
						{
							ArenaActor->Score -= 50;
							SnakeActor->SnakeElements[SnakeActor->SnakeElements.Num() - 1]->Destroy();
							SnakeActor->SnakeElements.RemoveAt(SnakeActor->SnakeElements.Num() - 1);
						}	
						GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
					}
					else
					{
						this->Destroy();
						for (int32 i = 0; i < SnakeActor->SnakeElements.Num(); i++)
						{
							ArenaActor->Score -= 50;
							SnakeActor->SnakeElements[i]->Destroy();
						}
						SnakeActor->Destroy();
						GEngine->AddOnScreenDebugMessage(2, 100.0f, FColor::Red, FString(" === YOU LOST === "), true, FVector2D(2.5f, 2.5f));
					}
				}
				if (bIsSpeedUpBonus)
				{
					
					this->Destroy();
					if (SnakeActor->GetMovementSpeed() > 0.15f)
					{
						SnakeActor->SetMovementSpeed(SnakeActor->GetMovementSpeed() - 0.05f);
						SnakeActor->SetActorTickInterval(SnakeActor->GetMovementSpeed());
						UE_LOG(LogTemp, Warning, TEXT("Snake Speed = %f"), SnakeActor->GetMovementSpeed());
						
						ArenaActor->Score += 50;
						GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
					}
				}
				if (bIsSlowDownBonus)
				{
					this->Destroy();
					if (SnakeActor->GetMovementSpeed() < 0.5f)
					{
						SnakeActor->SetMovementSpeed(SnakeActor->GetMovementSpeed() + 0.05f);
						SnakeActor->SetActorTickInterval(SnakeActor->GetMovementSpeed());
						UE_LOG(LogTemp, Warning, TEXT("Snake Speed = %f"), SnakeActor->GetMovementSpeed());
						
						ArenaActor->Score += 50;
						GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
					}
				}
			}
			else if (bIsBonusFood && !bUsualFood)
			{
				this->Destroy();
				SnakeActor->AddSnakeElement();
				ArenaActor->RefreshFoodSpawnMap();
				ArenaActor->Score += 100;
				GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
			}
			else if (bUsualFood)
			{
				ArenaActor->BonusCounter++;

				this->Destroy();
				SnakeActor->AddSnakeElement();
				ArenaActor->RefreshFoodSpawnMap();

				ArenaActor->Score += 100;
				GEngine->AddOnScreenDebugMessage(1, 100.0f, FColor::Yellow, FString("Score: ") + FString::FromInt(ArenaActor->Score), true, FVector2D(2.f, 2.f));
				ArenaActor->SpawnFood(false);

				if (ArenaActor->bBonusIsSpawned)
				{
					ArenaActor->FoodCounter++;
					if (ArenaActor->FoodCounter == 4)
					{
						GEngine->AddOnScreenDebugMessage(3, 100.0f, FColor::Yellow, FString("There are no bonuses"), true, FVector2D(1.5f, 1.5f));
					}
					else
					{
						GEngine->AddOnScreenDebugMessage(3, 100.0f, FColor::Yellow, FString("Food to refresh bonuses: ") + FString::FromInt(4 - ArenaActor->FoodCounter), true, FVector2D(1.5f, 1.5f));
					}					
					if (ArenaActor->FoodCounter >= 4)
					{
						if (ArenaActor->BonusesArray.Num() > 0)
						{
							for (int32 i = 0; i < ArenaActor->BonusesArray.Num(); i++)
							{
								ArenaActor->BonusesArray[i]->Destroy();
							}
							ArenaActor->BonusesArray.Empty();
							ArenaActor->FoodCounter = 0;
							ArenaActor->bBonusIsSpawned = false;
						}
					}
				}

				if (ArenaActor->BonusCounter == 2)
				{
					ArenaActor->BonusCounter = 0;
					ArenaActor->SpawnBonuses();
				}
			}			
		}
	}
}

void AFood::FoodAnimation()
{
	FRotator TempRotation = this->GetActorRotation();
	if (bIsSpeedUpBonus || bIsSlowDownBonus || bUsualFood || bIsBonusFood)
	{	}
	else
	{
		this->SetActorRotation(TempRotation - FRotator(0, 3.5, 0));
	}

	if (LimitCounter <= 10)
	{
		LimitCounter++;
		TempLocation += FVector(0, 0, 2.f);
		this->SetActorLocation(TempLocation);
	}
	else if (LimitCounter <= 30 && LimitCounter > 10)
	{
		LimitCounter++;
		TempLocation -= FVector(0, 0, 2.f);
		this->SetActorLocation(TempLocation);
	}
	else if (LimitCounter <= 40 && LimitCounter > 30)
	{
		LimitCounter++;
		TempLocation += FVector(0, 0, 2.f);
		this->SetActorLocation(TempLocation);
	}
	else
	{
		LimitCounter = 0;
		TempLocation = InitialLocation;
	}
}


void AFood::SetBonus()
{
	bIsBonus = true;
}

void AFood::SetInitialLocation(FVector Location)
{
	InitialLocation = Location;
	TempLocation = Location;
}

void AFood::GetArena(AArena* ArenaPtr)
{
	ArenaActor = ArenaPtr;
}

void AFood::GetSnake(ASnakeBase* SnakePtr)
{
	SnakeActor = SnakePtr;
}
