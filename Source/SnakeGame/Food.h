// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class AArena;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	AArena* ArenaActor;
	ASnakeBase* SnakeActor;

	FVector InitialLocation;
	FVector TempLocation;

	int32 LimitCounter = 0;

	bool bUsualFood = true;
	bool bIsBonus = false;
	bool bIsFoodBombBonus = false;
	bool bIsBonusFood = false;
	bool bIsPoisonousFood = false;
	bool bIsSpeedUpBonus = false;
	bool bIsSlowDownBonus = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void GetArena(AArena* ArenaPtr);
	void GetSnake(ASnakeBase* SnakePtr);
	void FoodAnimation();

	void SetInitialLocation(FVector Location);
	void SetBonus();

};