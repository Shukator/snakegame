// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Arena.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(2);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementSpeed == 0)
	{

	}
	else
	{
		Move();
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize * 100, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	//LastSnakeLocation = SnakeElements[0]->GetActorLocation();

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	CurrentSnakeLocation = SnakeElements[0]->GetActorLocation();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::GetArena(AArena* ArenaPtr)
{
	ArenaActor = ArenaPtr;
}

void ASnakeBase::SetMovementSpeed(float Speed)
{
	if (Speed >= 0)
	{
		MovementSpeed = Speed;
	}
	else
	{
		MovementSpeed = 0;
	}
}

float ASnakeBase::GetMovementSpeed()
{
	return MovementSpeed;
}

bool ASnakeBase::CheckNextMove(EMovementDirection Direction)
{
	bool CanMove = true;
	FVector ElementSizeDistance;
	switch (Direction)
	{
	case EMovementDirection::UP:
		ElementSizeDistance = FVector(100,0,0);
		if (CurrentSnakeLocation + ElementSizeDistance == SnakeElements[1]->GetActorLocation())
		{
			CanMove = false;
		}
		break;
	case EMovementDirection::DOWN:
		ElementSizeDistance = FVector(-100, 0, 0);
		if (CurrentSnakeLocation + ElementSizeDistance == SnakeElements[1]->GetActorLocation())
		{
			CanMove = false;
		}
		break;
	case EMovementDirection::LEFT:
		ElementSizeDistance = FVector(0, 100, 0);
		if (CurrentSnakeLocation + ElementSizeDistance == SnakeElements[1]->GetActorLocation())
		{
			CanMove = false;
		}
		break;
	case EMovementDirection::RIGHT:
		ElementSizeDistance = FVector(0, -100, 0);
		if (CurrentSnakeLocation + ElementSizeDistance == SnakeElements[1]->GetActorLocation())
		{
			CanMove = false;
		}
		break;
	default:
		break;
	}
	return CanMove;
}
