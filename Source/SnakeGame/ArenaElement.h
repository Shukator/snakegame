// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaElement.generated.h"

class UStaticMeshComponent;
class AArena;

UCLASS()
class SNAKEGAME_API AArenaElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArenaElement();

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* ArenaElement;

	/*bool bIsTopElementFree = true;
	bool bIsDownElementFree = true;
	bool bIsRightElementFree = true;
	bool bIsLeftElementFree = true;*/
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
