// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Arena.generated.h"

class AArenaElement;
class ASnakeElementBase;
class AFood;
class ASnakeBase;

USTRUCT()
struct FArenaWall
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		int32 VerticalLineNum;

	UPROPERTY(EditAnywhere)
		int32 ElementToBegin;

	UPROPERTY(EditAnywhere)
		int32 NumOfElements;

	UPROPERTY()
		FVector ElementPosition;

	UPROPERTY(EditAnywhere)
		bool bIsWallHorizontal = true;
	UPROPERTY(EditAnywhere)
		bool bIsWallVertical;
};

UCLASS()
class SNAKEGAME_API AArena : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AArena();

	UPROPERTY(EditAnywhere)
		TSubclassOf<AArenaElement> ArenaElement;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AArenaElement> ArenaWallElement;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ASnakeElementBase> SnakeElemClass;

	//Food and bonuses
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> FoodBombBonus;
	//For food elements that are splitted from the bomb bonus
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> FoodBombSplits;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> PoisonousFood;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> SpeedUpBonus;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> SlowDownBonus;

	TArray<AFood*> BonusesArray;

	//For FoodSpawnMap(ArenaMap on Snake level(Z coordinate = 100))
	TArray<FVector> TempArenaMap;

	TArray<FVector> FoodSpawnMap;
	TArray<FVector> TempFoodSpawnMap;
	//Food and bonuses-

	UPROPERTY(EditAnywhere)
		int32 ArenaWidth = 2;
	UPROPERTY(EditAnywhere)
		int32 ArenaHeight = 2;
	UPROPERTY(EditAnywhere)
		TArray<FArenaWall> ArenaWallsSet;

	UPROPERTY(EditAnywhere)
		TSubclassOf<ASnakeBase> SnakeClass;

	//For Food file
	AArena* ArenaActor;

	AFood* FoodActor;

	ASnakeBase* SnakeActor;
		FVector LastSnakeLocation;
		FVector NewSnakeLocation;	

	ASnakeElementBase* SnakeElem;

	AArenaElement* ArenaElem;

	//Array of arena floor elements location
	TArray<TArray<FVector>> ArenaMap;
	//Array of arena walls location
	TArray<FVector> ArenaWallsMap;

	//Array of arena walls
	TArray<AArenaElement*> ArenaWalls;

	//For foodmap debugging
	TArray<AArenaElement*> NewArenaElements;
	TArray<FVector> SnakeElementsLocation;

	//For food spawn
	int32 RandomCoordinate;
	int32 MapHeight = 0;
	int32 MapWidth = 0;
	//counter of eaten food needed to destroy all bonuses
	int32 FoodCounter = 0;
	//counter of eaten food needed to spawn bonus
	int32 BonusCounter = 0;
	int32 One = 1;
	int32 NotOne = 0;
	//Player score
	int32 Score = 0;

	FVector FirstHorizontalElement;
	FVector FirstVerticalElement;
	FVector TrueFirstElement;

	FVector LastHorizontalElement;
	FVector LastVerticalElement;
	FVector TrueLastElement;

	bool bBonusIsSpawned = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION()
		void SetArenaSize(int32 Width, int32 Height);

	void DrawHorizontalWall(int32 LineNum, int32 HorizontalElement, int32 NumOfElements);

	void DrawVerticalWall(int32 LineNum, int32 VerticalElement, int32 NumOfElements);

	void DrawAllWalls();

	void DrawWallFencing();

	void SpawnObstacles();


	//Not now
	/*
	//Checks one side of the wall (1 - Top, 2 - Down, 3 - Right, 4 - Left, Else - nothing)
	void CheckOneSide(int32 NumOfSide);
	void CheckWallsSides();*/

	ASnakeBase& CreateSnakeActor();

	ASnakeBase* GetSnake();

	void SetArena(AArena* ArenaPtr);

	void SetFoodSpawnMap();

	void SpawnFood(bool IsBonusFood);

	void SpawnBonus(TSubclassOf<AFood> FoodSubclass);
	//FoodBomb - 1; Poison - 2; SpeedUp - 3; SlowDown - 4
	void SpawnBonuses(int32 BonusNumber);
	//Randomly spawns bonuses
	void SpawnBonuses();

	//For food spawning area check(creates visible map of every block, where food can spawn)
	void FoodMapCreate();

	void RefreshFoodSpawnMap();
	bool CheckBonusesLocation(FVector FoodMapLocation);
	bool CheckSnakeLocation(FVector FoodMapLocation);

	void GetSnakeElementsLocation(TArray<ASnakeElementBase*> SnakeElems);
private:
	float ElementSize;
};
